<?php

namespace SimpleSaml\Controller;

use App\Controller\AppController as BaseController;
use Cake\Core\Configure;

class ConnectController extends BaseController
{
    /**
     * Set up component
     */
    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('SimpleSaml.SimpleSaml');
        $this->loadModel('User');
    }

    /**
     * beforeFilter method
     *
     * @return void
     */
    public function beforeFilter(\Cake\Event\Event $event)
    {
        parent::beforeFilter($event);
        $this->Auth->allow(['login']);
    }

    /**
     * login method
     *
     * @return void
     */
    public function login()
    {
        $this->request->session()->delete('Auth');
        $config = Configure::read('SimpleSaml');
        if (isset($config['testUser'])) {
            $userdata['Value'] = $config['testUser'];
            $authData = [];
        } else {
            $this->SimpleSaml->requireAuth();
            $userdata = $this->SimpleSaml->getAuthData('saml:sp:NameID');
            $authData = $this->SimpleSaml->getAuthDataArray();
        }

        $key = $this->getKey('LionLogin', $authData['Attributes']);
        $username = $authData['Attributes'][$key][0];
        // $username = str_replace(['LL/', 'LL\\'], ['', ''], $userdata['Value']);
        $this->request->data = [
            'username' => $username,
            'password' => time(),
            'saml' => true
        ];
        $user = $this->Auth->identify();
        if ($user) {
            $this->Auth->setUser($user);

            // clear messages
            $this->request->session()->delete('Flash');

            // force to url in config
            if (isset($config['redirectLogin'])) {
                return $this->redirect($config['redirectLogin']);
            }
            // redirect to reqested url
            return $this->redirect($this->Auth->redirectUrl());
        }

        $this->set('data', $authData);
        $this->set('username', $username);
    }

    /**
     * logout method
     *
     * @return void
     */
    public function logout()
    {
        // Default to SAML logout
        if (!$this->Auth->user('saml')) {
            return $this->redirect(['controller' => 'users', 'action' => 'logout', 'plugin' => false]);
        }
        return $this->SimpleSaml->logout();
    }

    protected function getKey($pattern, $input)
    {
        foreach ($input as $key => $val) {
            if (strpos($key, $pattern) !== false) {
                return $key;
            }
        }
    }
}
