<?php
namespace SimpleSaml\Controller\Component;

use Cake\Controller\Component;
use Cake\Utility\Hash;

class SimpleSamlComponent extends Component
{
    /**
     * Default configuration
     *
     * @var array
     */
    protected $_defaultConfig = [
        'authSource' => 'publicis-sp',
        'samlPath' => '/var/www/html/fidello/saml',
    ];

    /**
     * AuthSource reference
     *
     * @var object
     */
    protected $_as = null;

    /**
     * Constructor
     *
     * @param \Cake\Controller\ComponentRegistry $registry A ComponentRegistry this component can use to lazy load its components
     * @param array $settings Settings passed via controller
     */
    public function __construct(\Cake\Controller\ComponentRegistry $registry, array $config = [])
    {
        $this->_defaultConfig = Hash::merge($this->_defaultConfig, $config);
    }

    /**
     * Main execution method. Handles redirecting of invalid users, and processing
     * of login form data.
     *
     * @param Cake\Event\Event $event
     * @return bool
     */
    public function startup(\Cake\Event\Event $event)
    {
        extract($this->_defaultConfig);
        try {
            $autoload = $samlPath . DS . 'lib' . DS . "_autoload.php";

            // Autoload simplesamlphp classes.
            if(!file_exists($autoload)) {
                throw(new Exception("simpleSAMLphp lib loader file does not exist: " . $autoload));
            }

            include_once($autoload);
            $this->_as = new \SimpleSAML_Auth_Simple($authSource);

        } catch (Exception $e) {
            // SimpleSAMLphp is not configured correctly.
            throw(new Exception("SSO authentication failed: ". $e->getMessage()));
            return;
        }
    }

    /**
     * __call method
     *
     * @return array
     */
    public function __call($name, $arguments)
    {
        if (!$this->_as) {
            return [];
        }
        return call_user_func_array([$this->_as, $name], $arguments);
    }

}
