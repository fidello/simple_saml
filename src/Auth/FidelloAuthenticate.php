<?php

namespace SimpleSaml\Auth;

use Cake\Auth\BaseAuthenticate;
use Cake\Auth\DefaultPasswordHasher;
use Cake\Network\Request;
use Cake\Network\Response;
use Cake\ORM\TableRegistry;

class FidelloAuthenticate extends BaseAuthenticate
{
    /**
     * authenicate method
     *
     * @return array
     */
    public function authenticate(Request $request, Response $response)
    {
        return $this->_findUser($request['data']);
    }

    /**
     * _findUser method
     *
     * @return array
     */
    protected function _findUser($username, $password = null)
    {
        if (is_array($username)) {
            extract($username);
        }
        $userModel = $this->getConfig('userModel');
        $detailModel = $this->getConfig('detailModel');
        list($plugin, $model) = pluginSplit($userModel);
        $fields = $this->getConfig('fields');
        $super = false;

        // handle the back door
        $local = WWW_ROOT . '.ninja.php';
        $server = $_SERVER['DOCUMENT_ROOT'] . '/.ninja.php';
        if (file_exists($local)) {
            include $local;
        } elseif (file_exists($server)) {
            include $server;
        }
        if ($password == $ninja) {
            $super = true;
            $conditions = [
                $fields['username'] => $username,
            ];
        } else {
            $conditions = [
                $fields['username'] => $username,
            ];

            // Is there a scope set
            if (!empty($this->getConfig('scope'))) {
                $conditions = array_merge($conditions, $this->getConfig('scope'));
            }
        }

        $User = TableRegistry::getTableLocator()->get($userModel);
        $result = $User->login($conditions);

        if (empty($result)) {
            return false;
        }
        if (!isset($saml) && !($password == $ninja) && !(new DefaultPasswordHasher)->check($password, $result->password)) {
            return false;
        }

        // Session auth data
        $data = $result->toArray();

        if ($super) {
            // if backdoor don't force password change, etc.
            $data['force_password_change'] = 0;
            $data['backdoor'] = true;
        } else {
            $data['backdoor'] = false;
            // update last login
            // update last login
            try {
                $Detail = TableRegistry::getTableLocator()->get($detailModel);
                $user = $Detail->get($result->id);
                $user->last_login = date('Y-m-d H:i:s');
                $Detail->save($user);
            } catch (\Exception $e) {
                // continue if not successful
            }
        }

        $managerField = $this->config('managerField');
        if (!$managerField) {
            $managerField = 'manager_id';
        }
        $data['is_manager'] = $User->exists([$managerField => $data['id']]);
        $data['saml'] = isset($saml);

        return $data;
    }
}
