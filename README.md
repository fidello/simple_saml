SimpleSaml Plugin for Publicis
Version: 4.0.0

# 4.0.0
Updates for php 7 and new saml

Default Path: /var/www/html/fidello/saml
Default AuthSource: publicis-sp

Passes username, password as time, and saml flag to request object.

Set Configure::write('SimpleSaml.testUser', '') to bypass the Saml for test. Only work if saml is installed.

NOTE: After login the Plugin redirects to /users/home

Instructions:

##Using the Plugin routes##

Enable in Application.php

```php
$this->addPlugin('SimpleSaml', ['routes' => true]);
```

##Using without Plugin routes##

##Use the Fidello Auth Component##

This is needed to handle the saml login.

```php
public $components = [
	'Auth' => [
		'authenticate' => [
			'SimpleSaml.Fidello' => [
				'userModel' => 'User',
				'fields' => ['username' => 'LionLogin'],
				'managerField' => 'manager_id

			]
		]
	],
];
```

Copy it if you need to modify it.

##Options##

```php
Configure::write('SimpleSaml', [
	'redirectLogin' => [
		'controller' => 'Employees',
		'action' => 'home',
		'plugin' => false,
		'prefix' => false
	],
]);
```

##TODOs##

3.5.4
remove after login warning because of redirect
add redirect url after login

3.5.3
Ninja fix

3.5.2
Removed 3.5.1 changes. Changed %s to {0} in login.ctp

3.5.1
Password checking was removed

3.5.0
CakePHP 3+ support

1.5.0

Strip LL/ from the username
Set up SimpleSaml config parameters

1.6.0

Fixes test user
Adds the FidelloAuthenticate component
