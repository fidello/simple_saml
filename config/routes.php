<?php
use Cake\Routing\RouteBuilder;
use Cake\Routing\Router;
use Cake\Routing\Route\DashedRoute;

Router::scope('/', ['plugin' => 'SimpleSaml'], function ($routes) {
    // Disable the '/' route to use this file
    Router::connect('/', ['controller' => 'connect', 'action' => 'login', 'plugin' => 'SimpleSaml']);
    Router::connect('/logout', ['controller' => 'connect', 'action' => 'logout', 'plugin' => 'SimpleSaml']);

    $routes->fallbacks();
});